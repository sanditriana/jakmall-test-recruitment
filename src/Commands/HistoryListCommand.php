<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Conn;
use LucidFrame\Console\ConsoleTable;

class HistoryListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show Calculator History';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->signature = 'history:list {commands=*add,divide,multiply,pow,substrack : Filter history by commands}';
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        $numbers = $this->getInput();
        $conn    = new Conn();
        $fetchs  = $this->getDataHistory($conn);

        if (empty($fetchs)) {
            $this->comment('History is empty');
        } else {
            $this->comment($this->createTable($fetchs));
        }

    }
    protected function getInput(): array
    {
        return $this->argument('commands');
    }

    /**
     * Transformation input console from array to string
     *
     * @var string
     */
    protected function TransformInput(): string
    {
        $string = '';
        foreach ($this->getInput() as $value) {
            $string .= "'$value'" . ',';
        }
        return rtrim($string, " ,");
    }

    /**
     * Get data from database
     *
     * @var string
     */
    protected function getDataHistory($conn): array
    {
        //Get single data
        $row      = 10;
        $getCount = $conn->fetch("select count(id) as jml from histories where command in (" . $this->TransformInput() . ")");

        if ($getCount['jml'] >= $row) {
            $jml   = $getCount['jml'] - $row;
            $limit = "limit $jml, $row";
        } else {
            $limit = "";
        }

        //Get multi row
        $fetchs = $conn->fetchAll("select * from histories where command in (" . $this->TransformInput() . ") order by id asc $limit");
        return $fetchs;
    }

    /**
     * Create table provide by vendor
     *
     * @var string
     */
    protected function createTable(array $datas = []): void
    {
        foreach ($datas[0] as $key => $value) {
            $headers[] = $key;
        }

        $table = new ConsoleTable();
        $table->setHeaders($headers);

        foreach ($datas as $key) {
            foreach ($key as $value) {
                $row[] = $value;

            }
            $table->addRow($row);
            $row = array();
        }

        $table->showAllBorders()->display();
    }
}
