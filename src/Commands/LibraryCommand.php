<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Conn;

class LibraryCommand extends Command
{
    public $operator;

    protected function getCommandVerb(string $command): string
    {
        return $command;
    }

    protected function getCommandPassiveVerb(string $passiveCommand): string
    {
        return $passiveCommand;
    }

    protected function ArgumentVerb(string $arg): string
    {
        //base
        return $arg;
    }
    protected function ArgumentPassiveVerb(string $argPassive): string
    {
        //exponen
        return $argPassive;
    }

    /**
     * Execute query insert for saving history
     *
     * @return void
     */
    protected function saveToHistory(string $command, string $desc, string $result, string $output): void
    {
        $conn = new Conn();
        $conn->queryClient("insert into histories(command,description,result,output) values
            ('$command', '$desc', '$result', '$output')");
    }

    protected function array_flatten(array $array = null)
    {
        $result = array();

        if (!is_array($array)) {
            $array = func_get_args();
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result = array_merge($result, array($key => $value));
            }
        }

        return $result;
    }

    protected function getInput(array $args): array
    {
        foreach ($args as $arg) {
            $returnArgs[] = $this->argument($arg);

        }

        $returnArgs = $this->array_flatten($returnArgs);
        return $returnArgs;
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->getOperator($this->operator);
        $glue     = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    protected function getOperator($operator): string
    {
        $this->operator = $operator;
        return $this->operator;
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        switch ($this->operator) {
            case "+":
                return $number1 + $number2;
                break;
            case "-":
                return $number1 - $number2;
                break;
            case "*":
                return $number1 * $number2;
                break;
            case "/":
                return $number1 / $number2;
            case "^":
                return pow($number1, $number2);
                break;
            default:
                return 'nothing operator';
        }
    }
}
