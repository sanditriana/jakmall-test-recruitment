<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\LibraryCommand;

class AddCommand extends LibraryCommand 
{
    /**
     * Global variable
     *
     * @var string
     */
    public $commandVerb;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->commandVerb = $this->getCommandVerb('add');

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $this->commandVerb,
            $this->getCommandPassiveVerb('added')
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($this->commandVerb));
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        $numbers     = $this->getInput(array('numbers'));
        $operator    = $this->getOperator('+');
        $description = $this->generateCalculationDescription($numbers);
        $result      = $this->calculateAll($numbers);

        $output = sprintf('%s = %s', $description, $result);
        $this->comment($output);
        $this->saveToHistory($this->commandVerb, $description, $result, $output);
    }
}
