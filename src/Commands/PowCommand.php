<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\LibraryCommand;

class PowCommand extends LibraryCommand
{
    /**
     * Global variable
     *
     * @var string
     */
    public $commandVerb;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->commandVerb        = $this->getCommandVerb('pow');
        $this->commandPassiveVerb = $this->getCommandPassiveVerb('exponent');
        
        $this->signature = sprintf(
            '%s {%s : The %s number} {%s : The %s number}',
            $this->commandVerb,
            $this->ArgumentVerb('base'),
            $this->ArgumentPassiveVerb('base'),
            $this->ArgumentVerb('exp'),
            $this->ArgumentPassiveVerb('exponent')
        );


        $this->description = sprintf('%s the given Numbers', ucfirst($this->commandPassiveVerb));
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        $numbers     = $this->getInput(array('base','exp'));
        $operator    = $this->getOperator('^');
        $description = $this->generateCalculationDescription($numbers);
        $result      = $this->calculateAll($numbers);

        $output = sprintf('%s = %s', $description, $result);

        $this->comment($output);

        $this->saveToHistory($this->commandVerb,$description,$result,$output);
    }

   
    // protected function getInput(): array
    // {   
    //     $base = $this->argument('base');
    //     $exp  = $this->argument('exp');

    //     return [$base,$exp];

    //     //return $this->argument(array('base','exp'));
    // }
}
