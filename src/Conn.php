<?php

namespace Jakmall\Recruitment\Calculator;

use AntonioKadid\MySql\DatabaseException;
use AntonioKadid\MySql\PdoConnection;

class Conn extends PdoConnection
{
	public $connection;

    public function __construct()
    {
    	try
	    {
	        $this->connection = new PdoConnection('localhost', 3306, 'jakmall-test', 'root', 'root');

	    } catch (DatabaseException $exception) {
	        
	        echo $exception->getmessage();
	    
	    }
    }

    public function queryClient($string)
    {
    	$this->connection->execute($string);

    	$this->connection->commit();
    }

    public function fetchAll($query,array $params = array())
    {
    	return $this->connection->query($query, $params);
    }

    public function fetch($query,array $params = array())
    {
        return $this->connection->querySingle($query, $params);
    }
}