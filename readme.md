# Jakmall Calculator

## Database requirement
- create database on localhost
- setting credential in src/Conn.php
- file sql attached in /database.sql

## Software Requirements
- Docker

## Vendor installation
```
./composer install
```
## Run the Calculator
```
./calculator
```
